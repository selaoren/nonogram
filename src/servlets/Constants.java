package servlets;


public class Constants {
    public static final String USERNAME = "username";
    public static final String USER_NAME_ERROR = "username_error";
    static final String USERTYPE = "playertype";

    static final String GAMEDETAILS = "gameDetails";
    static final String FILLED = "filled";
    static final String EMPTY = "empty";
    static final String UNKNOWN = "unknown";
    static final String UNDO = "undo";
    static final String REDO = "redo";
    static final String TURNDONE = "turnDone";
    static final String BOARD = "board";
    static final String CHECK_GAME_START = "checkGameStart";
    static final String SYSTEM_MESSAGE = "systemMessage";
    static final String LEAVE_ROOM = "leaveRoom";
    static final String RESET_TO_LAST_MOVE = "resetToLastMove";
    static final String NUM_OF_ALL_MOVES = "numOfAllMoves";
    static final String UndoMoveWithoutChangeState = "undoMoveWithoutChangeState";
    static final String RedoMoveWithoutChangeState = "redoMoveWithoutChangeState";
}