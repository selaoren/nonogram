package GameEngine;


import java.util.LinkedList;

/**
 * Created by moran on 03/08/2016.
 */
public class PlayerManager {

    public enum PlayerType {
        Human,
        Computer,
        Spectator;
    }

    private final String name;
    private final PlayerType playerType;
    private  float score = 0;
    private int numOfAllMoves = 0;
    private final RandomMoveGenerator randomMoveGenerator;
    private  Board board; //TODO : copy


    PlayerManager(String name, Board board, PlayerType playerType) {
        this.name = name;
        this.board = board;
        this.playerType = playerType;
        if(playerType == PlayerType.Computer){
            this.randomMoveGenerator = new RandomMoveGenerator(board);
        }
        else{
            this.randomMoveGenerator = null;
        }
    }

    //region getters

    int getNumOfAllMoves() { return  numOfAllMoves;}

    float getScore() {
        return this.score;
    }

    public final String getName() {
        return  name;
    }
    final PlayerType getPlayerType(){
        return playerType;
    }


    public final Board getBoard() {
        return board;
    }
    //endregion

    @Override
    public String toString(){
        return String.format("%1s - %2s", playerType, name);
    }

    //region package public methods

    LinkedList<Triplet<Integer, Integer, Board.BoardSign>> undoMove() { //returns false if not possible
        LinkedList<Triplet<Integer, Integer, Board.BoardSign>> moves = board.applyUndoOperation();
        if(moves == null || moves.size() == 0){
            return null;
        }
        numOfAllMoves--;
        score = board.getScore();
        return moves;
    }

    LinkedList<Triplet<Integer, Integer, Board.BoardSign>> redoMove() {  //returns false if not possible
        LinkedList<Triplet<Integer, Integer, Board.BoardSign>> moves = board.applyRedoOperation();
        if(moves == null || moves.size() == 0){
            return null;
        }
        numOfAllMoves++;
        score = board.getScore();
        return moves;
    }

    boolean doMove(LinkedList<Triplet<Integer, Integer, Board.BoardSign>> moves) {
        boolean retValue = false;
        board.markBoard(moves);
        numOfAllMoves++;
        score = board.getScore();
        if (score == 100){
            retValue = true;
        }
        return  retValue;
    }

    void runComputerPlayerMove(){
        RandomMoveGenerator.RandomChoice randomChoice = randomMoveGenerator.drawMoveOrUndo();
        if(randomChoice == RandomMoveGenerator.RandomChoice.Move){
            LinkedList<Triplet<Integer, Integer, Board.BoardSign>> moves = randomMoveGenerator.makeRandomMoves();
            if(moves != null) {
                doMove(moves);
            }
        }
        else if(randomChoice == RandomMoveGenerator.RandomChoice.Undo){
            undoMove();
        }
    }

    //endregion



    //region will be used for replay backwards and forward bonus

    public void resetToLastMove(int reviewOffset){
            while (reviewOffset != 0){
                redoMoveWithoutChangeState();
                reviewOffset++;
            }
    }

    public void undoMoveWithoutChangeState() {
         board.applyUndoOperation();
    }

    public void redoMoveWithoutChangeState() {
         board.applyRedoOperation();
    }
    //endregion
}
