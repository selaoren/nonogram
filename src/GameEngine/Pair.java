package GameEngine;

/**
 * Created by s on 31/10/2016.
 */
public class Pair<K,V> {

    private K key;

    public K getKey() { return key; }

    private V value;

    public V getValue() { return value; }

    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }
}
