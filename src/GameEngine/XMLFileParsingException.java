package GameEngine;

/**
 * Created by moran on 11/08/2016.
 */
class XMLFileParsingException extends  RuntimeException {
    public XMLFileParsingException() {

    }

    XMLFileParsingException(String message) {
        super(message);
    }

    public XMLFileParsingException(Throwable cause) {
        super(cause);
    }

    public XMLFileParsingException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public String getMessage()  {
        return super.getMessage();
    }
}