package GameEngine;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by moran on 9/1/2016.
 */
class PerfectBlock {
    private final int rows;
    private final int cols;
    private final Board.BoardSign[][] board;
    private final List<List<Pair<Integer, Boolean>>> rowsBlocks;
    private final List<List<Pair<Integer, Boolean>>> columnsBlocks;
    private final List<List<Pair<Integer, Integer>>> rowsPossibleBlocksRange = new ArrayList<>();
    private final List<List<Pair<Integer, Integer>>> columnPossibleBlockRange = new ArrayList<>();


    PerfectBlock(Board.BoardSign[][] board, List<List<Pair<Integer, Boolean>>> rowsBlocks,   List<List<Pair<Integer, Boolean>>> columnsBlocks) {
        this.board = board;
        this.rowsBlocks = rowsBlocks;
        this.columnsBlocks = columnsBlocks;
        this.rows = rowsBlocks.size();
        this.cols = columnsBlocks.size();
        initializeRowsColumnsRange();
    }

    private void initializeRowsColumnsRange() {
        initializeRowsColumnsRange (rowsBlocks, rowsPossibleBlocksRange, cols);
        initializeRowsColumnsRange(columnsBlocks, columnPossibleBlockRange, rows);
    }

    private void initializeRowsColumnsRange(List<List<Pair<Integer, Boolean>>> blocks, List<List<Pair<Integer, Integer>>> possibleBlocksRange, int size) {
        int start, end;

        for (List<Pair<Integer, Boolean>> list : blocks) {
            List<Pair<Integer, Integer>> singlePossibleBlock = new ArrayList<>();
            for (int j = 1; j <= list.size(); j++) {
                if (j == 1)
                    start = 0;
                else {
                    start = 0;
                    for (int i = 1; i <= j - 1; i++) {
                        start += list.get(i - 1).getKey() + 1;
                    }
                }
                if (j == list.size())
                    end = size - 1;
                else {
                    end = size - 1;
                    for (int i = j + 1; i <= list.size(); i++) {
                        end += -list.get(i - 1).getKey() - 1;
                    }
                }
                singlePossibleBlock.add(new Pair<>(start, end));
            }
            possibleBlocksRange.add(singlePossibleBlock);
        }
    }

    void updatePerfectBlocks() {
        updatePerfectBlocks(rowsBlocks, rows, cols, rowsPossibleBlocksRange, true);
        updatePerfectBlocks(columnsBlocks, cols, rows, columnPossibleBlockRange, false);
    }

    private void updatePerfectBlocks(List<List<Pair<Integer, Boolean>>> blocks, int height, int length, List<List<Pair<Integer, Integer>>> possibleBlocksRange, Boolean isRow) {
        resetPerfectBlocks(blocks);
        for (int currRowOrCol = 0; currRowOrCol < height; currRowOrCol++) {
            List<Pair<Integer, Integer>> segments = new ArrayList<>(); //segment = consequent filled squares
            List<List<Integer>> possibleBlocksInSegment = new ArrayList<>();
            updateSegments(length, isRow, currRowOrCol, segments);
            if (!checkSegmentLegality(segments, blocks.get(currRowOrCol)))
                continue;
            if (!updatePossibleBlocksInSegment(possibleBlocksInSegment, segments, blocks.get(currRowOrCol), possibleBlocksRange.get(currRowOrCol)))
                continue;
            decideCorrectBlocks(possibleBlocksInSegment);
            decidePerfectBlocks(possibleBlocksInSegment, segments, blocks.get(currRowOrCol), length, currRowOrCol, isRow);
        }
    }



    private void decidePerfectBlocks(List<List<Integer>> possibleBlocksInSegment, List<Pair<Integer, Integer>> segments, List<Pair<Integer, Boolean>> blocks, int length,  int currRowOrCol, Boolean isRow) {
        for (int i = 0; i < possibleBlocksInSegment.size(); i++) {  // TODO : aaa
            if (possibleBlocksInSegment.get(i).size() == 1) {
                Pair<Integer,Integer> segment = segments.get(i);
                if (segment.getKey() == 0 && segment.getValue() != length - 1) {
                    Board.BoardSign sign = isRow ? board[currRowOrCol][segment.getValue() + 1] : board[segment.getValue() + 1][currRowOrCol];
                    if(sign != Board.BoardSign.Empty)
                        continue;
                }
                else if(segment.getKey() != 0 && segment.getValue() == length - 1) {
                    Board.BoardSign sign = isRow ? board[currRowOrCol][segment.getKey() - 1] : board[segment.getKey() - 1][currRowOrCol];
                    if(sign != Board.BoardSign.Empty)
                        continue;
                }
                else if(segment.getKey() != 0 && segment.getValue() != length - 1) {
                    Board.BoardSign signRight = isRow ? board[currRowOrCol][segment.getValue() + 1] : board[segment.getValue() + 1][currRowOrCol];
                    Board.BoardSign signLeft = isRow ? board[currRowOrCol][segment.getKey() - 1] : board[segment.getKey() - 1][currRowOrCol];
                    if (signRight != Board.BoardSign.Empty || signLeft != Board.BoardSign.Empty)
                        continue;
                }
                blocks.set(possibleBlocksInSegment.get(i).get(0), new Pair<>(blocks.get(possibleBlocksInSegment.get(i).get(0)).getKey(), true));
            }
        }
    }


    private void resetPerfectBlocks(List<List<Pair<Integer, Boolean>>> blocks) {
        for (List<Pair<Integer, Boolean>> currBlocks : blocks) {
            for (int i = 0; i < currBlocks.size(); i++) {
                currBlocks.set(i, new Pair<Integer, Boolean>(currBlocks.get(i).getKey(), false));
            }
        }
    }

    private void updateSegments(int length, boolean isRow, int currRowOrCol, List<Pair<Integer, Integer>> segments) {
        for (int i = 0; i < length; i++) {
            Board.BoardSign boardSign = isRow ? board[currRowOrCol][i] : board[i][currRowOrCol];
            if (boardSign == Board.BoardSign.Filled) {
                int start = i;
                while (i < length && boardSign == Board.BoardSign.Filled) {
                    i++;
                    if (i < length) {
                        boardSign = isRow ? board[currRowOrCol][i] : board[i][currRowOrCol];
                    }
                }
                int end = i - 1;
                segments.add(new Pair<>(start, end));
            }
        }
    }


    private Boolean checkSegmentLegality(List<Pair<Integer, Integer>> segments, List<Pair<Integer, Boolean>> blocks) {
        int i = 0;
        for (Pair<Integer, Integer> segment : segments) {
            int lenSegment = segment.getValue() - segment.getKey() + 1; //get the size of one segment
            Boolean found = false;
            while (i < blocks.size() && !found) {
                if (blocks.get(i).getKey() == lenSegment)               //compare it with the block size
                    found = true;                                       //if found and in the right order, look for the next
                i++;
            }
            if (!found)
                return false;                                           //not found or not in order
        }
        return true;
    }

    private Boolean updatePossibleBlocksInSegment(List<List<Integer>> possibleBlocksInSegment, List<Pair<Integer, Integer>> segments, List<Pair<Integer, Boolean>> blocks, List<Pair<Integer, Integer>> possibleBlocksRange) {
        for (Pair<Integer, Integer> segment : segments) {
            List<Integer> possibleBlocks = new LinkedList<>();
            for (int j = 0; j < blocks.size(); j++) {
                if (blocks.get(j).getKey() == segment.getValue() - segment.getKey() + 1 && //check size
                    possibleBlocksRange.get(j).getKey() <= segment.getKey() &&             //check block in the right range
                    possibleBlocksRange.get(j).getValue() >= segment.getValue()) {
                    possibleBlocks.add(j);
                }
            }
            if (possibleBlocks.size() == 0)                     // if there's one mismatch then no block is perfect
                return false;
            possibleBlocksInSegment.add(possibleBlocks);
        }
        return true;
    }

    // remove smaller than current values from others
    // the result will be the certain values for each block
    private void decideCorrectBlocks(List<List<Integer>> possibleBlocksInSegment) {
        for (int i = 0; i < possibleBlocksInSegment.size(); i++) {
            if (possibleBlocksInSegment.get(i).size() == 0) {
                return;
            }
            int min = possibleBlocksInSegment.get(i).get(0); // the minimal index of possible blocks
            for (int start = 0; start < i; start++) {
                possibleBlocksInSegment.set(start, possibleBlocksInSegment.get(start).stream().filter(num -> num < min).collect(Collectors.toList()));
            }
            for (int start = i + 1; start < possibleBlocksInSegment.size(); start++) {
                possibleBlocksInSegment.set(start, possibleBlocksInSegment.get(start).stream().filter(num -> num > min).collect(Collectors.toList()));
            }
        }
    }
}
