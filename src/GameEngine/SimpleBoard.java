package GameEngine;

import java.util.List;

/**
 * Created by moran on 10/7/2016.
 */
public class SimpleBoard {

    private final Board.BoardSign[][] board;
    private final List<List<Pair<Integer, Boolean>>> rowsBlocks;
    private final List<List<Pair<Integer, Boolean>>> columnsBlocks;

    public SimpleBoard(Board.BoardSign[][] board, List<List<Pair<Integer, Boolean>>> rowsBlocks, List<List<Pair<Integer, Boolean>>> columnsBlocks) {
        this.board = board;
        this.rowsBlocks = rowsBlocks;
        this.columnsBlocks = columnsBlocks;
    }
}
