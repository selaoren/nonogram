package GameEngine;

import java.util.LinkedList;

/**
 * Created by s on 06/08/2016.
 * this is the memento part of the memento design pattern for undo and redo
 * holds the state before the undo or redo
 */
class UndoFrame {
    private final LinkedList<Triplet<Integer,Integer, Board.BoardSign>> previousStates;

    UndoFrame() {
        previousStates = new LinkedList<>();
    }

    LinkedList<Triplet<Integer,Integer, Board.BoardSign>> getPreviousStates() {
        return previousStates;
    }

    void addPrevState(Integer row, Integer col, Board.BoardSign prevState){
        previousStates.add(new Triplet<>(row, col, prevState));
    }
}
