package GameEngine;

import java.util.*;

/**
 * Created by moran on 03/08/2016.
 */

public class Board implements Cloneable {

    public enum BoardSign {
        Unknown,
        Filled,
        Empty;

        @Override
        public String toString() {
            switch (this) {
                case Unknown:
                    return "?";
                case Filled:
                    return "+";
                case Empty:
                    return "-";
                default:
                    throw new IllegalArgumentException();
            }
        }
    }


    private final UndoCaretaker undoCaretaker;
    private final PerfectBlock perfectBlock;
    private final Board.BoardSign[][] board;
    private  List<List<Pair<Integer, Boolean>>> rowsBlocks;
    private  List<List<Pair<Integer, Boolean>>> columnsBlocks;
    private Board.BoardSign[][] solution;

     Board(ArrayList<List<Integer>> rowsBlocks, ArrayList<List<Integer>> columnsBlocks, BoardSign[][] solution) {
         this.solution = solution;
         this.board = new Board.BoardSign[rowsBlocks.size()][columnsBlocks.size()];
         this.undoCaretaker = new UndoCaretaker();
         translateListToPairList(rowsBlocks, columnsBlocks);
         resetBoard();
         this.perfectBlock = new PerfectBlock(board, this.rowsBlocks, this.columnsBlocks);
     }

    //region getters/setters


    public List<List<Pair<Integer, Boolean>>> getRowsBlocks() {
        return rowsBlocks;
    }

    public List<List<Pair<Integer, Boolean>>> getColumnsBlocks() {
        return columnsBlocks;
    }

    public Board.BoardSign[][] getBoard() {
        return board;
    }

    public int getRows() {
        return board.length;
    }

    public int getCols() {
        return board[0].length;
    }

    //endregion

    //region package public methods

    @Override
    protected Board clone() {
        return new Board(translatePairListToList(rowsBlocks), translatePairListToList(columnsBlocks), solution);
    }

    void markBoard(LinkedList<Triplet<Integer,Integer, Board.BoardSign>> moves) {
        UndoFrame frame = new UndoFrame();
        for (Triplet<Integer, Integer, Board.BoardSign> move : moves) {
            frame.addPrevState(move.getFirst(), move.getSecond(), board[move.getFirst()][move.getSecond()]);  //save old state to frame
            board[move.getFirst()][move.getSecond()] = move.getThird();                                       //update board with new state
        }

        perfectBlock.updatePerfectBlocks();

        undoCaretaker.pushUndoFrame(frame);
        undoCaretaker.resetRedoFrames();
    }

    LinkedList<Triplet<Integer,Integer, Board.BoardSign>> applyUndoOperation() {
        UndoFrame frame = undoCaretaker.popUndoFrame();
        if (frame == null)
            return null; //nothing will happen if no undo frames exist

        UndoFrame redoFrame = new UndoFrame(); //init the string
        updateBoardAndUndoFrame(frame, redoFrame);
        undoCaretaker.pushRedoFrame(redoFrame);
        return frame.getPreviousStates();
    }

    LinkedList<Triplet<Integer,Integer, Board.BoardSign>> applyRedoOperation() {
        UndoFrame frame = undoCaretaker.popRedoFrame();
        if (frame == null)
            return null; //nothing will happen if no redo frames exist

        UndoFrame undoFrame = new UndoFrame(); //init the string
        updateBoardAndUndoFrame(frame, undoFrame);
        undoCaretaker.pushUndoFrame(undoFrame);
        return frame.getPreviousStates();
    }

    private void resetBoard() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = Board.BoardSign.Unknown;
            }
        }
        undoCaretaker.resetAllFrames();
    }

    float getScore() {    //in 0-100 indicating %
        int countGoodSquares = 0;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (board[i][j] == solution[i][j]) {
                    countGoodSquares++;
                }
            }
        }

        return (float) countGoodSquares / (board.length * board[0].length) * 100;
    }

    //endregion

    //region private methods


    private void updateBoardAndUndoFrame(UndoFrame frame, UndoFrame undoFrame) {
        LinkedList<Triplet<Integer,Integer, Board.BoardSign>> moves = frame.getPreviousStates();
        for (Triplet<Integer, Integer, Board.BoardSign> move : moves) {
            undoFrame.addPrevState(move.getFirst(), move.getSecond(), board[move.getFirst()][move.getSecond()]);  //save old state
            board[move.getFirst()][move.getSecond()] = move.getThird();                                           //update board with new state
        }
        perfectBlock.updatePerfectBlocks();
    }

    private void translateListToPairList(ArrayList<List<Integer>> rowsBlocks, ArrayList<List<Integer>> columnsBlocks) {
        this.rowsBlocks = translateListToPairList(rowsBlocks);
        this.columnsBlocks=  translateListToPairList(columnsBlocks);
    }

    private List<List<Pair<Integer, Boolean>>> translateListToPairList(List<List<Integer>>  list) {
        List<List<Pair<Integer, Boolean>>> retValue = new ArrayList<>();
        for (List<Integer> currBlocks : list) {
            List<Pair<Integer, Boolean>> rowOrColumnBlocks = new ArrayList<>();
            for (Integer block : currBlocks) {
                rowOrColumnBlocks.add(new Pair<>(block, false));
            }
            retValue.add(rowOrColumnBlocks);
        }
        return retValue;
    }

    private ArrayList<List<Integer>> translatePairListToList(List<List<Pair<Integer, Boolean>>> list){
        ArrayList<List<Integer>> resultList = new ArrayList<>();

        for (List<Pair<Integer, Boolean>> pairs : list) {
            List<Integer> currList = new ArrayList<>();
            for (Pair<Integer, Boolean> pair : pairs) {
                currList.add(pair.getKey());
            }
            resultList.add(currList);
        }
        return resultList;
    }

    //endregion
}


