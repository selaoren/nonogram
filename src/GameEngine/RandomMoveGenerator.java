package GameEngine;

import java.util.LinkedList;
import java.util.Random;

/**
 * Created by s on 07/08/2016.
 * creates moves randomly, chooses a random amount of UNKNOWN squares and mark or unmark them
 * until there are no more UNKNOWN squares
 */
class RandomMoveGenerator {
    private final Random rand = new Random();
    private final Board board;

    RandomMoveGenerator(Board board) {
        this.board = board;
    }

    enum RandomChoice {
        Move,
        Undo
    }

    RandomChoice drawMoveOrUndo(){
        int undoPossibility = 15;
        int draw = rand.nextInt(undoPossibility);
        if(draw == 0){
            //do undo with a possibility of 1 /  undoPossibility
            return RandomChoice.Undo;
        }
        else{
            //do a regular move
            return RandomChoice.Move;
        }
    }

    LinkedList<Triplet<Integer, Integer, Board.BoardSign>> makeRandomMoves(){
        LinkedList<Pair<Integer, Integer>> unmarkedSquares = createSquareChoices();
        if(unmarkedSquares.size() == 0){
            //if no more squares are unknown, return nothing
            return null;
        }

        int choiceAmount = rand.nextInt(unmarkedSquares.size() <= 2 ? 1 : unmarkedSquares.size() / 3) + 1;    //choose at least one square, third of unknowns so it will make more moves
        LinkedList<Triplet<Integer, Integer, Board.BoardSign>> moves = new LinkedList<>();
        Board.BoardSign sign =  rand.nextBoolean() ? Board.BoardSign.Filled : Board.BoardSign.Empty;
        for(int i = 0; i< choiceAmount; i++){
            Pair<Integer, Integer> square = unmarkedSquares.get(rand.nextInt(unmarkedSquares.size()));
            unmarkedSquares.remove(square);
            moves.add(new Triplet<>(square.getKey(), square.getValue(), sign));
        }

        return moves;
    }

    private LinkedList<Pair<Integer, Integer>> createSquareChoices(){    //get all the coordinates that are unknown
        LinkedList<Pair<Integer, Integer>> unmarkedSquares = new LinkedList<>();
        for (int i = 0; i < board.getBoard().length; i++) {
            for (int j = 0; j < board.getBoard()[0].length; j++) {
                if(board.getBoard()[i][j] == Board.BoardSign.Unknown)
                    unmarkedSquares.add(new Pair<>(i,j));
            }
        }
        return unmarkedSquares;
    }
}
