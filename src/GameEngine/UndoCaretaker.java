package GameEngine;

import java.util.Stack;

/**
 * Created by s on 06/08/2016.
 * this is the caretaker part of the memento design pattern of undo and redo
 */
class UndoCaretaker {

    private Stack<UndoFrame> undoFrames;
    private Stack<UndoFrame> redoFrames;

    UndoCaretaker() {
        undoFrames = new Stack<>();
        redoFrames = new Stack<>();
    }

    void pushUndoFrame(UndoFrame frame){
        undoFrames.push(frame);
    }

    void pushRedoFrame(UndoFrame frame){
        redoFrames.push(frame);
    }

    UndoFrame popUndoFrame(){    //get undo frame to restore prev state
        if(undoFrames.isEmpty()){
            return null;
        }

        return undoFrames.pop();
    }

    UndoFrame popRedoFrame(){    //call this if you want to redo
        if(redoFrames.isEmpty()){
            return null;
        }

        return redoFrames.pop();
    }

    void resetRedoFrames(){  //call this after every move by user was made
        redoFrames.clear();
    }

    void resetAllFrames(){
        redoFrames.clear();
        undoFrames.clear();
    }
}
