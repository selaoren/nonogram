package GameEngine;

import jaxb.schema.generated.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.util.*;

public class XMLReader{

    private static final String JAXB_XML_GAME_PACKAGE_NAME = "jaxb.schema.generated";
    private static final String ROW = "row";
    private static final String COLUMN = "column";

     public void loadXML(InputStream inputStream, GameManager gameManager, RoomsManager roomsManager) throws Exception {
         if(inputStream.available() == 0){
             throw new XMLFileParsingException("file is empty");
         }
        ArrayList<List<Integer>> rowsBlocks = new ArrayList<>();
        ArrayList<List<Integer>> columnsBlocks = new ArrayList<>();
        Board.BoardSign [][] solution;

        try {
            GameDescriptor gameDescriptor = deserializeFrom(inputStream);
            GameDescriptor.Board.Definition definitionBoard = gameDescriptor.getBoard().getDefinition();

            int rows = definitionBoard.getRows().intValue();        // update rows
            int columns = definitionBoard.getColumns().intValue();  // update columns
            if (rows < 1 || rows > 99 || columns < 1 || columns > 99) {
                throw new XMLFileParsingException("rows, columns values must be between 5 to 99");
            }

            List<Slice> readSlices = definitionBoard.getSlices().getSlice();
            updateBlocks(readSlices, rowsBlocks, rows, columns, ROW);         //update Rows Blocks
            updateBlocks(readSlices, columnsBlocks, columns, rows, COLUMN);   //update Columns Blocks
            Solution readSolution = gameDescriptor.getBoard().getSolution();
            solution = getSolution(readSolution, rows, columns);              //update solution

            //update board
            gameManager.setPrototypeBoard(new Board(rowsBlocks, columnsBlocks, solution));

            //update multi player data
            GameDescriptor.DynamicMultiPlayers multiPlayerData = gameDescriptor.getDynamicMultiPlayers();
            gameManager.setGameTitle(readGameTitle(multiPlayerData, roomsManager));
            gameManager.setRounds(readRounds(multiPlayerData));
            gameManager.setTotalPlayers(readTotalPlayers(multiPlayerData));
        } catch (JAXBException e) {
            throw new XMLFileParsingException("Bad XML File");  //all other reasons
        }
    }

    private String readGameTitle(GameDescriptor.DynamicMultiPlayers multiPlayerData, RoomsManager roomsManager) {
        String gameTitle = multiPlayerData.getGametitle();
        if(gameTitle.trim().equals("")) {
            throw new XMLFileParsingException("Game must have a title");
        }
        else if(roomsManager.getRoomList().stream().anyMatch(roomInfo -> roomInfo.getGameTitle().equals(gameTitle))) {
            throw new XMLFileParsingException("Game title already exist");
        }
        return gameTitle.trim();
    }

    private int readTotalPlayers(GameDescriptor.DynamicMultiPlayers multiPlayerData) {
        int totalPlayers = Integer.parseInt(multiPlayerData.getTotalPlayers());
        if (totalPlayers < 0) {
            throw new XMLFileParsingException("Game has to have at least one player");
        }
        return totalPlayers;
    }

    private int readRounds(GameDescriptor.DynamicMultiPlayers multiPlayerData) {
        int rounds = Integer.parseInt(multiPlayerData.getTotalmoves());
        if (rounds < 0) {
            throw new XMLFileParsingException("Number of rounds has to be larger than 0");
        }
        return rounds;
    }


    private   Board.BoardSign [][]  getSolution(Solution readSolution, int rows, int columns) {
        Board.BoardSign [][] solution = new Board.BoardSign[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j <columns; j++) {
                solution[i][j] = Board.BoardSign.Empty;
            }
        }

        List<Square> squaresSolution = readSolution.getSquare();
        for (Square square : squaresSolution) {
            int row = square.getRow().intValue();
            int column = square.getColumn().intValue();
            if (row >= 1 && row <= rows && column >= 1 && column <= columns) {
                if (solution[row - 1][column - 1] == Board.BoardSign.Filled) {
                    throw new XMLFileParsingException("Solution contains the same square multiple times");
                }
                solution[row - 1][column - 1] = Board.BoardSign.Filled;
            } else {
                throw new XMLFileParsingException("Solution's values are beyond the boundaries of the board");
            }
        }
        return  solution;
    }

    private void updateBlocks(List<Slice> readSlices, ArrayList<List<Integer>> blocks, int count, int maxSumBlocks, String str) {
        List<Slice> slices = new ArrayList<>();
        for (Slice slice : readSlices) {
            if (slice.getOrientation().equals(str))
                slices.add(slice);
        }

        if (slices.size() != count) {
            throw new XMLFileParsingException(String.format("Num of %1$ss must be %2$d", str, count));
        }

        slices.sort(SENIORITY_ORDER);
        for (Slice theSlice : slices) {
            int sumBlocks = 0;
            ArrayList<Integer> currentBlocks = new ArrayList<Integer>();
            String theBlocks = theSlice.getBlocks();
            String[] splitByCommas = theBlocks.split(",");
            for (String currentSplitComma : splitByCommas) {
                int lenBlock = Integer.parseInt(currentSplitComma.trim());
                currentBlocks.add(lenBlock);
                sumBlocks += lenBlock;
            }

            if (sumBlocks > maxSumBlocks - splitByCommas.length + 1) {
                throw new XMLFileParsingException(String.format("Max sum of blocks in %1$s %2$d must be %3$d", theSlice.getOrientation(), theSlice.getId(), maxSumBlocks - splitByCommas.length + 1));
            }

            if (theSlice.getOrientation().equals(str))
                blocks.add(currentBlocks);
        }
    }

    private static final Comparator<Slice> SENIORITY_ORDER = (s1, s2) -> s1.getId().compareTo(s2.getId());

    private GameDescriptor deserializeFrom(InputStream in) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(JAXB_XML_GAME_PACKAGE_NAME);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        return (GameDescriptor) unmarshaller.unmarshal(in);
    }
}
